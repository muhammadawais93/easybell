<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class UserUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'       => ['sometimes', 'string', 'max:70'],
            'first_name' => ['sometimes', 'string', 'max:70'],
            'last_name'  => ['sometimes', 'string', 'max:70'],
        ];
    }
}
