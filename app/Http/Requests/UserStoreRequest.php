<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class UserStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'       => ['required', 'string', 'max:70'],
            'first_name' => ['required', 'string', 'max:70'],
            'last_name'  => ['required', 'string', 'max:70'],
        ];
    }
}
