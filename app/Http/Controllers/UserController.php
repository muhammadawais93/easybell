<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;

class UserController extends Controller
{
    public function index()
    {
        return response()->json(User::paginate());
    }

    public function show(User $user)
    {
        return response()->json($user);
    }

    public function store(UserStoreRequest $request)
    {
        return response()->json(User::create($request->validated()));
    }

    public function update(User $user, UserUpdateRequest $request)
    {
        return response()->json($user->update($request->validated()));
    }

    public function destroy(User $user)
    {
        return response()->json($user->delete());
    }
}
