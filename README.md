## Setup Project

Project is setup with docker laravel, in order to run project:

- ddev start
- ddev composer install
- ddev launch

Migrate and seed the DB

- Rename .env.example to .env
- ddev artisan migrate:fresh --seed

Install node Modules

-  ddev exec npm install

Create build

-  ddev exec npm run production

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
