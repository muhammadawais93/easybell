// require('./bootstrap');

import {createApp} from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './vue/App.vue';
import { routes } from './vue/routes/routes';

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
});

createApp(App)
.use(router)
.use(VueAxios, axios)
.mount("#app")
