import Overview from '../components/Overview.vue';
import CreateUser from '../components/CreateUser.vue';
import EditUser from '../components/EditUser.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: Overview
    },
    {
        name: 'create',
        path: '/create',
        component: CreateUser
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditUser
    }
];
